.PHONY: clean prepare prepare_audio
default: all

# the name of the article file (without extension)
ARTICLE = article

# the audio files (without extension)
AUDIO = XC183559-Accgent-Adjuv_Poland_PSZ_2014_06_23_4498 \
XC185619-Northern_Goshawk2014-7-3-1 \
XC308382-Jastrzab_Acipiter_gentilis_co_Poland_Jarek_Matusiak_200324 \
XC309594 \
XC331689-Goshawk_20160326_080226_prepared \
XC215431-50_Bois_de_Pau_3_mars_2015_vers_10h \
XC382023-Tegelerfliess_170717_0154_Accipiter

# run latex and output to pdf
all: prepare
	cd temp && \
	pdflatex $(ARTICLE) && \
	bibtex $(ARTICLE) && \
	pdflatex $(ARTICLE) && \
	pdflatex $(ARTICLE)
	mv -f temp/$(ARTICLE).pdf docs
	echo created docs/$(ARTICLE).pdf 

# Prepare audio files
wav:
	mkdir wav
        # generate 1 minute of silence to pad short files
	sox -n -r 22050 -c 1 wav/silence.wav trim 0 60
        # convert each file in AUDIO
	$(foreach var, ${AUDIO}, $(eval CURRENT_FILE:=${var}) $(convert_audio_cmd); )
        # remove the silence file
	rm wav/silence.wav

# draw the spectrograms
spectro: wav
	-mkdir spectro
	$(foreach var, ${AUDIO}, $(eval CURRENT_FILE:=${var}) $(draw_spectro_cmd); )

# draw the plots
plots/plotMonths.png:
	Rscript src/plotMonths.R data/dates.csv plots/plotMonths.png

# prepare the table and output to text file
plots/tableMonths.tex:
	cd src && Rscript tableMonths.R

# copy all files required by latex to the temp dir
prepare: plots/plotMonths.png plots/tableMonths.tex spectro
	mkdir -p temp
	cp docs/preamble.tex temp
	cp docs/$(ARTICLE).tex temp
	cp docs/$(ARTICLE).bib temp
	cp plots/*.png temp
	cp plots/*.tex temp
	$(foreach var, ${AUDIO}, \
		cp spectro/${var}.png temp; \
	)

# cleanup output and temp files
clean:
	rm -rf spectro
	rm -rf wav
	rm -f docs/$(ARTICLE).pdf
	rm -f plots/plotMonths.png
	rm -f plots/tableMonths.tex
	rm -f temp/*


###################################
#       Commands
###################################

# Call sox to convert mp3 to wav and trim it
MAX_AUDIO_LENGTH = 20 # seconds
define convert_audio_cmd
        # convert the mp3 to wav
	sox audio/${CURRENT_FILE}.mp3 -r 22050 -c 1 wav/${CURRENT_FILE}.wav
        # pad with silence and store in temp file
	sox wav/${CURRENT_FILE}.wav wav/silence.wav wav/${CURRENT_FILE}_temp.wav
        # trim from the beginning to MAX_AUDIO_LENGTH
	sox wav/${CURRENT_FILE}_temp.wav wav/${CURRENT_FILE}.wav trim 0 ${MAX_AUDIO_LENGTH}
        # remove the temp file
	rm wav/${CURRENT_FILE}_temp.wav
endef

# Call R to plot a spectrogram
define draw_spectro_cmd
	Rscript src/plotSpectrogram.R wav/${CURRENT_FILE}.wav spectro/${CURRENT_FILE}.png 
endef


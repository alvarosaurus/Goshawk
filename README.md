# Goshawk
These are data and R scripts for the article ["Vocalizations of the northern goshawk and their relation to the annual breeding cycle"](https://gitlab.com/alvarosaurus/Goshawk/raw/master/docs/article.pdf?inline=false).

## Installation
Dependencies:
```
r-base >= 3.3.3
r-base-dev >= 3.3.3
libsndfile1-dev >= 1.0.27-3
libsbsms-dev >= 2.0.2-2
libfftw3-dev >= 3.3.5-3
sox >= 14.4.1-5+b2
libsox-fmt-mp3 >= 14.4.1-5+b2
```
R-packges:
```
sound
tuneR
seewave
ggplot2
cowplot
```
A LaTeX distribution, e.g.
```
texlive-full >= 2014.20141024
```
Calling make with no arguments should build the article and save it to docs/article.pdf.
```
make
```

# Example: XC215431

Bernard Bousquet, [XC215431](http://www.xeno-canto.org/215431).

License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0

# Example: XC308382

Jarek Matusiak, 2016-03-24, [XC308382](http://www.xeno-canto.org/308382)

License:  Creative Commons Attribution-NonCommercial-ShareAlike 4.0

# Example: XC185619

Andrew Spencer, [XC185619](http://www.xeno-canto.org/185619).

License:  Creative Commons Attribution-NonCommercial-ShareAlike 4.0

# Example: XC309594

Jarek Matusiak, [XC309594](http://www.xeno-canto.org/309594).

License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0

# Example: XC382023

Alvaro Ortiz Troncoso, 2017-07-17, [XC382023](http://www.xeno-canto.org/382023).

License: Creative Commons Attribution-ShareAlike 4.0

# Example: XC331689

Lars Lachmann, 2016-03-26, [XC331689](http://www.xeno-canto.org/331689).

License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0

# Example: XC183559

Piotr Szczypinski, 2014-06-23, [XC18355](http://www.xeno-canto.org/183559)

License: Creative Commons Attribution-NonCommercial-NoDerivs 4.0

\input{preamble}

% ----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\setlength{\droptitle}{-4\baselineskip} % Move the title up

\pretitle{\begin{center}\Huge\bfseries} % Article title formatting
\posttitle{\end{center}} % Article title closing formatting
\title{Vocalisations of the northern goshawk and their relation to the annual breeding cycle} % Article title
\author{%
\textsc{Alvaro Ortiz-Troncoso}\thanks{Recordings by: Bernard Bousquet, Lars Lachmann, Jarek Matusiak, Alvaro Ortiz Troncoso,  Andrew Spencer, Piotr Szczypinski and others. The complete list of recordists can be found on http://www.xeno-canto.org/set/3277} \\[1ex] % Your name
\normalsize Museum für Naturkunde Berlin \\
\normalsize (Science in society department)\\ % Your institution
\normalsize \href{mailto:alvaro.ortiztroncoso@mfn.berlin}{alvaro.ortiztroncoso@mfn.berlin} % Your email address
}
\date{\today} % Leave empty to omit a date
\renewcommand{\maketitlehookd}{%
\begin{abstract}
  \noindent
  Goshawks are secretive birds, best detected by their vocalisations. Four common types of vocalisations were identified in literature and recordings of each of these were collected from an online source (Xeno-Canto). An examination of the distribution of the dates of the recordings confirms previous studies on the correspondence between vocalisation types and breeding cycle, however, female wailing calls seem to play a more important role during the courtship and territory establishment period than expected.
\end{abstract}
}

%----------------------------------------------------------------------------------------

\begin{document}

% Print the title
\maketitle

%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------

\section{Introduction}

\lettrine[nindent=0em,lines=3]{T}he characteristic habitats of the northern goshawk (\textit{Accipiter gentilis}) are extended conifer or deciduous forests with a dense canopy. Due to the bird's secretive nature, well-hidden nests and extended foraging area, it often goes undetected during surveys, except by its vocalisations (\citep{watson1999efficacy}.

Identifying the vocalisations of the goshawk is therefore a good method for detecting their presence \citep{roberson2001evaluating}. The Xeno-Canto website \footnote{http://xeno-canto.org} is an accessible source for crowd-sourced, good quality, freely distributed recordings of bird vocalisations. Recordings downloaded from Xeno-Canto were used to compile a key for identifying different types of calls during surveys, and a bayesian analysis was used to infer the type of interaction implied by the call, in relation to the annual breeding cycle.

%------------------------------------------------
\section{Previous work}

\subsection{Types of vocalisations}
\citet{gromme1935goshawk} describes the vocal interaction between nestlings and female as well as between male and female. \citet{schnell1958nesting} lists the calls of females, males and nestlings/fledglings, and provides a detailed description of the associated behaviour for each call. \citet{kennedy1993responsiveness} and \citet{roberson2001evaluating} conducted surveys using broadcasts of taped calls and used three types of calls in each study. \citet{roberson2001evaluating} distinguish between 3 con-specific calls (alarm call, male contact call and juvenile begging call). \citet{penteriani2001annual} lists six types of calls used by adults, and five types of calls used by fledglings. 

The types of call retained by most researchers are described in table \ref{table:commoncalls}.

\begin{table}
\caption{Common calls of the goshawk.}
\label{table:commoncalls}
\centering
\begin{tabu} to \linewidth{llX}
\toprule
 Call & Transcription & Description \\
\midrule
Single-note call
& \textit{kek...}
& Single-note call interpreted as mate contact call \citep{penteriani2001annual,schnell1958nesting}, male contact call \citep{roberson2001evaluating}, recognition call prior to food transfer \citep{schnell1958nesting}. \\
\midrule
Chattering & \textit{kek-kek-kek} & Adults and juveniles. Alarm call directed towards an intruder or predator \citep{kennedy1993responsiveness}. Call to attract mates during courtship \citep{penteriani2001annual}. Defense cackle \citep{schnell1958nesting}. Battle-cry \citep{gromme1935goshawk}. \\
\midrule
Female wailing call & \textit{whee-oo... whee-oo} & Exclusively a female call \citep{penteriani2001annual}, communication between members of a pair \citep{kennedy1993responsiveness}. Recognition of mate, upon food delivery during nesting \citep{penteriani2001annual,schnell1958nesting}. Female on guard, appeal for food \citep{gromme1935goshawk}. \\
\midrule
Juvenile call       & \textit{whee... whee... whee} & Fledged young, food begging and location call \citep{penteriani2001annual}. \\
\bottomrule
\end{tabu}
\end{table}

\subsection{Annual breeding cycle}
The annual breeding cycle as described by \citet{penteriani2001annual} is summarised in table \ref{table:breedingcycle}.

\begin{table}[H]
\caption{Goshawks' breeding cycle (based on \citet{penteriani2001annual}).}
\label{table:breedingcycle}
\centering
\begin{tabu} to \linewidth{llllll}
\toprule
Period & Non-breeding & Territory-building, courtship & Incubation & Nesting period & Fledging period \\
\midrule
Start  & September    &   February   &   April       &   May        &   July \\
End    & January      &   March      &   early May   &   June        &   August    \\
\bottomrule
\end{tabu}
\end{table}

%------------------------------------------------

\section{Methods}

For the first objective: compiling a key for identifying different types of calls, all recordings of northern goshawks with visual confirmation, a quality rating of „A“ and a Creative-Commons license were downloaded from the Xeno-Canto website. This data set has 20 recordings. Examples for each type of call were selected and for each selected recording, a spectrogram was plotted using the R library Seewave \citep{sueur2008seewave}.

For the second objective: examining the type of interaction implied by the calls in relation to the annual breeding cycle, the metadata of all recordings of goshawks on the Xeno-Canto website \footnote{http://www.xeno-canto.org/set/3277} was downloaded. After discarding low-quality recordings and recordings with incomplete metadata, this data set has 144 entries. The recordings in this data set were categorised in ”single-note”, ”chattering”, ”wailing” and ”juvenile” using the key above. The entries were aggregated by month and sub-sampled 100 times (bootstrap). A bayesian analysis was used to infer the probability that a given type of call corresponds to a season in the breeding cycle. Scripts and data set are available online \footnote{https://gitlab.com/alvarosaurus/Goshawk}.
%------------------------------------------------

\section{Results}

\subsection{Examples of distinct vocalisation types}
The following files are exemplary of distinct vocalisation types:

\subsubsection{One-note call}
One-note mate recognition calls play a role during territory establishment and nesting and are often followed by a chattering call \citep{penteriani2001annual}. One-note calls are given by the male to indicate its presence prior to food transfer during nesting, or when encountering the female \citep{schnell1958nesting}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC215431-50_Bois_de_Pau_3_mars_2015_vers_10h.png}
  \caption{Series of one-note calls. Bernard Bousquet, 2015-03-03 (http://www.xeno-canto.org/215431). License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\subsubsection{Chattering}

Chattering calls are the most common type of call. Chattering calls are uttered by males and females throughout the year. Chattering calls are used as a response against other goshawks entering the territory, for mobbing predators (owls) and seem to play a role during courtship \citep{penteriani2001annual}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC308382-Jastrzab_Acipiter_gentilis_co_Poland_Jarek_Matusiak_200324.png}
  \caption{Chattering as a con-specific call: Copulation. Recorded by: Jarek Matusiak, 2016-03-24 (http://www.xeno-canto.org/308382). License:  Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC185619-Northern_Goshawk2014-7-3-1.png}
  \caption{Chattering as inter-specific call: alarm call from a female goshawk, in presence of an owl. Recorded by: Andrew Spencer, 2014-07-03 (http://www.xeno-canto.org/185619). License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\subsubsection{Female wailing call}
Wailing calls are uttered by females. During incubation and nesting, females stay on or in proximity of the nest, while males hunt \citep{gromme1935goshawk,schnell1958nesting,penteriani2001annual}. Wailing calls have been interpreted as a food and location call used by females to demand food from males \citep{penteriani2001annual}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC309594.png}
  \caption{Female wailing call. Recorded by: Jarek Matusiak, 2016-03-24 (http://www.xeno-canto.org/309594). License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\subsubsection{Juvenile begging call}
The calls of fledglings resemble a higher pitched female wailing call. While fledging, young goshawks are fed by the adults while they climb in the branches around the nest \citep{schnell1958nesting}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC382023-Tegelerfliess_170717_0154_Accipiter.png}
  \caption{Fledgling call. Recorded by: Alvaro Ortiz Troncoso, 2017-07-17 (http://www.xeno-canto.org/382023). License: Creative Commons Attribution-ShareAlike 4.0}
\end{figure}

\subsection{Correspondence between calls and breeding cycle}
Recording counts and standard deviations were calculated for each month and call type (figure \ref{fig:plotmonths}). A bayesian analysis yielded the probability that a call type corresponds to a season of the breeding cycle (table \ref{table:conditional}).

Figure \ref{fig:plotmonths} shows the number of calls recorded for each month of the year. 

\begin{figure}[H]
  \includegraphics[width=\textwidth]{plotMonths.png}
  \caption{Recording counts per month.}
  \label{fig:plotmonths}
\end{figure}

Table \ref{table:conditional} lists the probability that each kind of call is associated with a certain period in the breeding cycle.

For example, if
* P(A) is the probability that a recording contains a single-note call.\\
* P(B) is the probability that a recording was made during the territory-building and courtship period (February-March).\\ 
then
P(B|A) is the probability that goshawks have entered the territory-building and courtship period if a single-note call was recorded. Since this probability is strong (P=0.63), it can be said that single-note calls are characteristic of the territory-building and courtship period.

\input{tableConditional.tex}

%------------------------------------------------
\section{Discussion}


\subsection{Territory-building and courtship period (February - March)}
Three types of calls: single-note, chattering and wailing, show a peak in March (figure \ref{fig:plotmonths}). This confirms previous observations \citep{penteriani2001annual}. Single-note calls appear to be strongly associated with territory-building and courtship (P=0.63); interestingly, wailing is associated with the courtship and territory establishment period (P=0.46); this association has to my knowledge not been documented in previous studies; chattering appears to play a role during this period, to a lesser degree (P=0.36) (table \ref{table:conditional}).

\subsection{Incubation (April)}
Goshawks are not particularly vocal during the incubation period (figure \ref{fig:plotmonths}).

\subsection{Nesting (May - June)}
Calls show a second peak around June, these can be interpreted as vocalisations associated with nesting (figure \ref{fig:plotmonths}). Nesting appears to be strongly associated with juvenile calls (P=0.52) and chattering (P=0.38), and to a lesser degree with wailing (P=0.36) (table \ref{table:conditional}).

\subsection{Fledging (July - August)}
The juvenile call peaks in June and July (figure \ref{fig:plotmonths}). Fledging is strongly associated with juvenile calls (P=0.48) (table \ref{table:conditional}).

\subsection{Dispersion and non-breeding period (September - January)}
The single-note and chattering plots show a smaller peak in October (figure \ref{fig:plotmonths}), corresponding to the period were young goshawks are dispersing from the nest area \citep{penteriani2001annual}. No vocalisations were recorded in November-December, which corresponds to published research \cite{penteriani2001annual}.

%------------------------------------------------

\section{Conclusions}
The Xeno-Canto website \footnote{http://xeno-canto.org} proved an accessible source for good quality recordings. The number of recordings of goshawks crowd-sourced on Xeno-Canto (144 recordings) exceeds the number of recordings in comparable databases, e.g. Tierstimmenarchiv \footnote{http://tierstimmenarchiv.de} (19 recordings) and The Macaulay Library \footnote{https://www.macaulaylibrary.org} (67 recordings).

A bayesian analysis yielded the conditional probability that a given type of call corresponds to a period of the breeding cycle. The conditional probabilities that four types of common calls (single-note, chattering, wailing and juvenile calls) correspond to five periods of the breeding cycle could be computed from 144 recordings contributed by 64 recordists.

The results correspond to previous findings obtained through traditional methods (direct observations, playback surveys), with one variation: vocalizations during the courtship period are more varied than reported, as the female wailing call seems to play a role during courtship and territory establishment.

%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------

%\renewcommand*{\bibname}{\section{References}}
%\bibliographystyle{plain}
%\bibliography{article}

% ----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\addcontentsline{toc}{chapter}{Bibliography}
\bibliographystyle{abbrvnat}
\bibliography{article}

%----------------------------------------------------------------------------------------

\end{document}

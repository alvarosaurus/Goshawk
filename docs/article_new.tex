\input{preamble}

% ----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\setlength{\droptitle}{-4\baselineskip} % Move the title up

\pretitle{\begin{center}\Huge\bfseries} % Article title formatting
\posttitle{\end{center}} % Article title closing formatting
\title{Vocalisations of the northern goshawk and their relation to the annual breeding cycle} % Article title
\author{%
\textsc{Alvaro Ortiz-Troncoso}\thanks{Recordings by: Bernard Bousquet, Lars Lachmann, Jarek Matusiak, Alvaro Ortiz Troncoso,  Andrew Spencer, Piotr Szczypinski and others. The complete list of recordists can be found on http://www.xeno-canto.org/set/3277} \\[1ex] % Your name
\normalsize Museum für Naturkunde Berlin \\
\normalsize (Science in society department)\\ % Your institution
\normalsize \href{mailto:alvaro.ortiztroncoso@mfn-berlin.de}{alvaro.ortiztroncoso@mfn-berlin.de} % Your email address
}
\date{\today} % Leave empty to omit a date
\renewcommand{\maketitlehookd}{%
\begin{abstract}
  \noindent
  Goshawks are secretive birds, best detected by their vocalisations. Four common types of vocalisations were identified in literature and recordings of each of these were collected from an online source (Xeno-Canto). An examination of the relative frequency of the recordings confirms previous studies on the correspondence between vocalisation types and breeding cycle however, female wailing calls seem to play a more important role during courtship than expected. The lack of recordings for the winter months hints that additional effort is needed, as direct observation shows that goshawks are vocal during this period.
\end{abstract}
}

%----------------------------------------------------------------------------------------

\begin{document}

% Print the title
\maketitle

%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------

\section{Introduction}

\lettrine[nindent=0em,lines=3]{O}nline technologies have facilitated the emergence of online communities with a focus on natural sciences. These communities regroup individuals around common interests and enable managing and sharing observations through connected devices, such as smart phones and computers. Many of these communities use digital images as their medium of choice, however, communities specialised in providing tools for crowd-sourcing audio recordings exist. The Xeno-Canto website is an accessible source for crowd-sourced, freely distributed recordings of bird vocalisations. Xeno-Canto aims to compile a collection of sounds of all bird taxa and make them available as a non-profit resource \footnote{https://www.xeno-canto.org/about/xeno-canto}.

Some bird species are most efficiently surveyed using identification techniques based on vocalisations. An example of such a bird is the northern goshawk (\textit{Accipiter gentilis}). The characteristic habitats of the northern goshawk are extended conifer or deciduous forests with a dense canopy. Due to the bird's secretive nature, well-hidden nests and extended foraging area, it often goes undetected during surveys, except by its vocalisations (\citep{watson1999efficacy}.

The northern goshawk presents therefore a suitable test case for evaluating whether crowd-sourced audio recordings are a suitable method for compiling a resource for identifying and surveying birds. The completeness of the repertoire, time of year was evaluated by comparing them to published findings obtained through direct observations from a hide or foot searches.

Identifying the vocalisations of the goshawk is therefore a good method for detecting their presence \citep{roberson2001evaluating}. The Xeno-Canto website \footnote{http://xeno-canto.org} is an accessible source for crowd-sourced, good quality, freely distributed recordings of bird vocalisations. Recordings downloaded from Xeno-Canto were used to compile a key for identifying different types of calls during surveys, and a bayesian analysis was used to infer the type of interaction implied by the call, in relation to the annual breeding cycle.

Additionally, the results were compared to published findings obtained through direct observations from a hide or foot searches. Crowd-sourcing observations online, as practised on the Xeno-Canto website, is a relatively new development, therefore it was interesting to find out that the results obtained by examining crowd-sourced data correspond to the results yielded by traditional methods, with the exception of female wailing calls, which seem to play a more important role during courtship than expected. Additionally it was noted that neither crowd-sourced observations nor published research provide satisfactory data for the winter months.

%------------------------------------------------
\section{Previous work}

\subsection{Methods for studying goshawk behaviour}
\subsubsection{Observations from a hide}
Several researchers have attempted to study goshawks by building a hide in a tree in the vicinity of a nest. \citet{gromme1935goshawk} built a hide at 7.5 meters distance from a goshawks' nest. He collected photographic and film material from May until June. \citet{schnell1958nesting} built a hide at 15 meters distance from a nest. Observation started in mid June and lasted for five weeks. The young were marked and measured. Additionally, \citet{schnell1958nesting} collected pellets and some of the prey was weighted and returned. Both researchers transcribed vocalisations and noted the associated behaviour.

Observations from a hide interfered considerably with the behaviour of the birds, and the researchers were attacked by the female goshawk on several occasions while climbing into the hide. Nevertheless, \citet{gromme1935goshawk} provides a detailed account of the vocalisations during the nesting and dependency periods. \citet{schnell1958nesting} describes the different roles played by the female and the male while providing food for the nestlings, and describes the associated mate contact calls.

\subsubsection{Surveys using playback}
\citet{kennedy1993responsiveness} and \citet{roberson2001evaluating} assessed the use of recorded vocalisations for detecting goshawks during the courtship (April), breeding (June) and fledging periods (July-August), with the aim to improve survey techniques. \citet{kennedy1993responsiveness} studied 27 nesting sites. Observations were made at distances between 100 and 700 meters from the nests. \citet{roberson2001evaluating} broadcast recorded calls at 25 nesting sites, at distances between 100 and 325 meters.

The recorded calls used in the surveys were adult alarm, female wailing and juvenile food-begging calls \citep{kennedy1993responsiveness} or adult alarm and contact calls and juvenile begging calls \citep{roberson2001evaluating}. These studies show that surveys that used recorded con-specific calls had a higher rate of detection of goshawk nesting sites than surveys without them.   

\subsubsection{Acoustic studies}
\citet{penteriani1999dawn,penteriani2001annual} collected recordings from 8 pairs of goshawks for a year. Observations were made at a distance of 100 meters to the nests. \citet{penteriani1999dawn,penteriani2001annual} studied the length of the calls, and their correspondence to the goshawk's annual breeding cycle and daily activity patterns.

\subsection{Types of vocalisations}
\citet{gromme1935goshawk} describes the vocal interaction between nestlings and female as well as between male and female. \citet{schnell1958nesting} lists the calls of females, males and nestlings/fledglings, and provides a detailed description of the associated behaviour for each call. \citet{kennedy1993responsiveness} and \citet{roberson2001evaluating} used three types of calls in both studies. \citet{roberson2001evaluating} distinguish between 3 con-specific calls (alarm call, male contact call and juvenile begging call). \citet{penteriani2001annual} lists six types of calls used by adults, and five types of calls used by fledglings.

The types of call retained by most researchers are described in table \ref{table:commoncalls}.

\begin{table}
\caption{Common calls of the goshawk.}
\label{table:commoncalls}
\centering
\begin{tabu} to \linewidth{llX}
\toprule
 Call & Transcription & Description \\
\midrule
Single-note call
& \textit{kek...}
& Single-note call interpreted as mate contact call \citep{penteriani2001annual,schnell1958nesting}, male contact call \citep{roberson2001evaluating}, recognition call prior to food transfer \citep{schnell1958nesting}. \\
\midrule
Chattering & \textit{kek-kek-kek} & Adults and juveniles. Alarm call directed towards an intruder or predator \citep{kennedy1993responsiveness}. Call to attract mates during courtship \citep{penteriani2001annual}. Defense cackle \citep{schnell1958nesting}. Battle-cry \citep{gromme1935goshawk}. \\
\midrule
Female wailing call & \textit{whee-oo... whee-oo} & Exclusively a female call \citep{penteriani2001annual}, communication between members of a pair \citep{kennedy1993responsiveness}. Recognition of mate, upon food, delivery, dismissal \citep{penteriani2001annual,schnell1958nesting}. Female on guard, appeal for food \citep{gromme1935goshawk}. \\
\midrule
Juvenile call       & \textit{whee... whee... whee} & Fledged young, food begging and location call \citep{penteriani2001annual}. \\
\bottomrule
\end{tabu}
\end{table}

\subsection{Annual breeding cycle}
The annual breeding cycle as described by \citet{penteriani2001annual} is summarised in table \ref{table:breedingcycle}.

\begin{table}[H]
\caption{Goshawks' breeding cycle (based on \citet{penteriani2001annual}).}
\label{table:breedingcycle}
\centering
\begin{tabu} to \linewidth{llllll}
\toprule
Period & Non-breeding & Courtship & Incubation & Nesting period & Fledging period \\
\midrule
Start  & September    &   February   &   April       &   May        &   July \\
End    & January      &   March      &   early May   &   June        &   August    \\
\bottomrule
\end{tabu}
\end{table}

%------------------------------------------------

\section{Methods}

For the first objective: compiling a key for identifying different types of calls, all recordings of northern goshawks with visual confirmation, a quality rating of „A“ and a Creative-Commons license were downloaded from the Xeno-Canto website. This data set has 20 recordings. Examples for each type of call were selected and for each selected recording, a spectrogram was plotted using the R library Seewave \citep{sueur2008seewave}.

For the second objective: examining the type of interaction implied by the calls in relation to the annual breeding cycle, the metadata of all recordings of goshawks on the Xeno-Canto website \footnote{http://www.xeno-canto.org/set/3277} was downloaded. After discarding low-quality recordings and recordings with incomplete metadata, this data set has 144 entries. The recordings in this data set were categorised in ”single-note”, ”chattering”, ”wailing” and ”juvenile” using the key above. The entries were aggregated by month and sub-sampled 100 times (bootstrap). A bayesian analysis was used to infer the probability that a given type of call corresponds to a season in the breeding cycle. Scripts and data-set are available online \footnote{https://github.com/alvarosaurus/Goshawk}.
%------------------------------------------------

\section{Results}

\subsection{Examples of distinct vocalisation types}
The following files are exemplary of distinct vocalisation types:

\subsubsection{One-note call}
One-note mate recognition calls play a role during territory establishment and nesting and are often followed by a chattering call \citep{penteriani2001annual}. One-note calls are given by the male to indicate its presence prior to food transfer during nesting, or when encountering the female \citep{schnell1958nesting}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC215431-50_Bois_de_Pau_3_mars_2015_vers_10h.png}
  \caption{Series of one-note calls. Bernard Bousquet, 2015-03-03 (http://www.xeno-canto.org/215431). License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\subsubsection{Chattering}

Chattering calls are the most common type of call. Chattering calls are uttered by males and females throughout the year. Chattering calls are used as a response against other goshawks entering the territory, for mobbing predators (owls) and seem to play a role during courtship \citep{penteriani2001annual}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC308382-Jastrzab_Acipiter_gentilis_co_Poland_Jarek_Matusiak_200324.png}
  \caption{Chattering as a con-specific call: Copulation. Recorded by: Jarek Matusiak, 2016-03-24 (http://www.xeno-canto.org/308382). License:  Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC185619-Northern_Goshawk2014-7-3-1.png}
  \caption{Chattering as inter-specific call: alarm call from a female goshawk, in presence of an owl. Recorded by: Andrew Spencer, 2014-07-03 (http://www.xeno-canto.org/185619). License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\subsubsection{Female wailing call}
Wailing calls are uttered by females. During incubation and nesting, females stay on or in proximity of the nest, while males hunt \citep{gromme1935goshawk,schnell1958nesting,penteriani2001annual}. Wailing calls have been interpreted as a food and location call used by females to demand food from males \citep{penteriani2001annual}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC309594.png}
  \caption{Female wailing call. Recorded by: Jarek Matusiak, 2016-03-24 (http://www.xeno-canto.org/309594). License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\subsubsection{Juvenile begging call}
The calls of fledglings resemble a higher pitched female wailing call. While fledging, young goshawks are fed by the adults while they climb in the branches around the nest \citep{schnell1958nesting}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC382023-Tegelerfliess_170717_0154_Accipiter.png}
  \caption{Fledgling call. Recorded by: Alvaro Ortiz Troncoso, 2017-07-17 (http://www.xeno-canto.org/382023). License: Creative Commons Attribution-ShareAlike 4.0}
\end{figure}

\subsubsection{Complex interactions}
Complex interactions have been noted between pairs of adults and between adults and nestlings \citep{schnell1958nesting}. These interactions involve many different types of calls.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC331689-Goshawk_20160326_080226_prepared.png}
  \caption{Pair vocalisations during the courtship period. Recorded by: Lars Lachmann, 2016-03-26 (http://www.xeno-canto.org/331689). License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=\textwidth]{XC183559-Accgent-Adjuv_Poland_PSZ_2014_06_23_4498.png}
  \caption{Juveniles and adult female at nest. Recorded by: Piotr Szczypinski, 2014-06-23 (http://www.xeno-canto.org/183559). License: Creative Commons Attribution-NonCommercial-NoDerivs 4.0}
\end{figure}

\subsection{Correspondence between calls and breeding cycle}
All recordings of goshawks on the Xeno-Canto website were categorised in "single-note", "chattering", "wailing" and "juvenile" using the key above. The entries were aggregated by month and sub-sampled 100 times (bootstrap). Recording counts and standard deviations were calculated for each month and call type (figure \ref{fig:plotmonths}). A bayesian analysis yielded the probability that a call type corresponds to a season of the breeding cycle (table \ref{table:conditional}).

\begin{figure}[H]
  \includegraphics[width=\textwidth]{plotMonths.png}
  \caption{Recording counts per month.}
  \label{fig:plotmonths}
\end{figure}

\input{tableConditional.tex}

%------------------------------------------------
\section{Discussion}

\subsection{Territory-building and courtship period (February - March)}
Calls show a peak in March (figure \ref{fig:plotmonths}). This confirms previous observations \citep{penteriani2001annual}. Territory-building and courtship appears strongly associated with single-note calls, chattering and wailing (table \ref{table:conditional}).

\subsection{Incubation (April)}
Goshawks are not particularly vocal during the incubation period (figure \ref{fig:plotmonths}).

\subsection{Nesting (May - June)}
Calls show a second peak around June, these can be interpreted as vocalisations associated with nesting (figure \ref{fig:plotmonths}). Nesting appears to be strongly associated with juvenile calls and chattering, and to a lesser degree with wailing (table \ref{table:conditional}).

\subsection{Fledging (July - August)}
The juvenile call peaks in June and July (figure \ref{fig:plotmonths}). Fledging is strongly associated with juvenile calls (table \ref{table:conditional}).

\subsection{Dispersion and non-breeding period (September - January)}
The single-note and chattering plots show a smaller peak in October (figure \ref{fig:plotmonths}), corresponding to the period were young goshawks are dispersing from the nest area \citep{penteriani2001annual}. No vocalisations were recorded in November-December, which corresponds to published research \cite{penteriani2001annual}.

%------------------------------------------------

\section{Conclusions}
The Xeno-Canto website \footnote{http://xeno-canto.org} proved an accessible source for good quality recordings. The number of recordings of goshawks on Xeno-Canto (144 recordings) exceeds the number of recordings in comparable databases, e.g. Tierstimmenarchiv \footnote{http://tierstimmenarchiv.de} (19 recordings) and The Macaulay Library \footnote{https://www.macaulaylibrary.org} (67 recordings).

A bayesian analysis yielded the conditional probability that a given type of call corresponds to a period of the breeding cycle. The conditional probabilities that four types of common calls (single-note, chattering, wailing and juvenile calls) correspond to five periods of the breeding cycle could be computed from 144 recordings crowd-sourced by 64 recordists.

The results correspond to previous findings obtained through traditional methods, with one variation: vocalizations during the courtship period are more varied than reported, as wailing seems to play a role during courtship.

\section{Further research}

Although no recordings or published research were available for the months November-December at the time of writing, there are several recordings dated from December 2017, that show that goshawks are vocal during the winter months \footnote{Giovanni Pari https://macaulaylibrary.org/asset/21693711} \footnote{Kenny Frisch https://macaulaylibrary.org/asset/77337601} \footnote{Alvaro Ortiz Troncoso http://www.xeno-canto.org/395460}. Further research could examine whether this hints to a bias in the data or to changing habits. The case for changing habits is particularly interesting because, as noted by \citet{artmann2015der}, the past decades have seen an increase in the urban goshawk population, which in turn could hint at a significant change in the goshawk's environment.

%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------

%\renewcommand*{\bibname}{\section{References}}
%\bibliographystyle{plain}
%\bibliography{article}

% ----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\addcontentsline{toc}{chapter}{Bibliography}
\bibliographystyle{abbrvnat}
\bibliography{article}

%----------------------------------------------------------------------------------------

\end{document}
